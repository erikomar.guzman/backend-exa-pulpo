import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsuariosModule } from './usuarios/usuarios.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CatalogosModule } from './catalogos/catalogos.module';
import { AuthModule } from './auth/auth.module';
import { TransporteModule } from './transporte/transporte.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'gufe.postgres.database.azure.com',
      port: 5432,
      username: 'gufeuser@gufe',
      password: 'Gufe$01$',
      database: 'postgres',
      autoLoadEntities: true,
      synchronize: true,
    }),
    UsuariosModule,
    CatalogosModule,
    AuthModule,
    TransporteModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
