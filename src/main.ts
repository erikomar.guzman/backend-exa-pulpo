import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { TypeORMExceptionFilter } from './filters/typeormexceptions.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // CONFIGURACION DE SWAGGER
  const config = new DocumentBuilder()
    .setTitle('Transpote-exa')
    .setDescription('Evaluación técnica')
    .setVersion('1.0')
    .addTag('plp')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  // FILTRO PARA MAEJO DE ERRORES GLOBAL
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalFilters(new TypeORMExceptionFilter());
  app.enableCors();
  await app.listen(3000);
}
bootstrap();
