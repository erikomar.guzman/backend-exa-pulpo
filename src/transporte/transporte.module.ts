import { Module } from '@nestjs/common';
import { TransporteController } from './transporte.controller';
import { TransporteService } from './transporte.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Color } from 'src/catalogos/entity/Color.entity';
import { Marca } from 'src/catalogos/entity/Marca.entity';
import { Vehiculo } from './entity/Vehiculo.entity';
import { Novedades } from './entity/Novedades.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Color, Marca, Vehiculo, Novedades])],
  controllers: [TransporteController],
  providers: [TransporteService],
  exports: [TransporteService]
})
export class TransporteModule {}
