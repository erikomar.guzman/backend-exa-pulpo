import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeleteResult } from 'typeorm';
import { Novedades } from './entity/Novedades.entity';
import { Vehiculo } from './entity/Vehiculo.entity';
import { NovedadesRepository } from './repository/novedades.repository';
import { VehiculoRepository } from './repository/vehiculo.repository';
import { VehiculoDTO } from './dtos/vehiculo.dto';
import { plainToClass } from 'class-transformer';
import { NovedadesDTO } from './dtos/novedades.dto';

@Injectable()
export class TransporteService {
    private vehiculoRepository: VehiculoRepository;
    private novedadesRepository: NovedadesRepository;
  
    constructor(
      
      @InjectRepository(Vehiculo)
      private readonly repository: Repository<Vehiculo>,

      @InjectRepository(Novedades)
      private readonly repositoryNovedades: Repository<Novedades>,
    
      ) {
      this.vehiculoRepository = new VehiculoRepository(this.repository);
      this.novedadesRepository = new NovedadesRepository(this.repositoryNovedades);
    }
  
    //Vehiculo
    async obtenerVehiculos(): Promise<Vehiculo[]> {
      const vehiculos: Vehiculo[] = await this.vehiculoRepository.todoVehiculo();
      return vehiculos;
    }
  
    async obtenerVehiculo(id: number): Promise<Vehiculo> {
      return await this.vehiculoRepository.unicoVehiculo(id);
    }
  
    async agregarVehiculo(vehiculoDto: VehiculoDTO): Promise<Vehiculo> {
      let vehiculo = plainToClass(Vehiculo, vehiculoDto);
      vehiculo = await this.vehiculoRepository.agregarVehiculo(vehiculo);
      return vehiculo;
    }
  
    async actualizarVehiculo(id: number, vehiculoDto: VehiculoDTO): Promise<Vehiculo> {
      let updVehiculo = plainToClass(Vehiculo, vehiculoDto);
      updVehiculo = await this.vehiculoRepository.actualizarVehiculo(id, updVehiculo);
      return updVehiculo;
    }
  
    async eliminarVehiculo(id: number): Promise<DeleteResult> {
      const resp = await this.vehiculoRepository.eliminarVehiculo(id);
      return resp;
    }
  
    //NovedadesS
    async obtenerNovedades(): Promise<Novedades[]> {
      const novedadess: Novedades[] = await this.novedadesRepository.todoNovedades();
      return novedadess;
    }
  
    async obtenerNovedad(id: number): Promise<Novedades> {
      return await this.novedadesRepository.unicoNovedades(id);
    }
  
    async agregarNovedades(novedadesDto: NovedadesDTO): Promise<Novedades> {
      let novedades = plainToClass(Novedades, novedadesDto);
      novedades = await this.novedadesRepository.agregarNovedades(novedades);
      return novedades;
    }
  
    async actualizarNovedades(id: number, novedadesDto: NovedadesDTO): Promise<Novedades> {
      if (id == null) {
        throw new BadRequestException();
      }
      let updNovedades = plainToClass(Novedades, novedadesDto);
      updNovedades = await this.novedadesRepository.actualizarNovedades(id, updNovedades);
      return updNovedades;
    }
  
    async eliminarNovedades(id: number): Promise<DeleteResult> {
      const resp = await this.novedadesRepository.eliminarNovedades(id);
      return resp;
    }
  }
  