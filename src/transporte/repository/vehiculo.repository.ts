import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Vehiculo } from "../entity/Vehiculo.entity";
import { DeleteResult, Repository } from 'typeorm';

@Injectable()
export class VehiculoRepository{
    constructor(
        @InjectRepository(Vehiculo)
        private readonly vehiculoRepository: Repository<Vehiculo>,
      ) {}
      async todoVehiculo(): Promise<Vehiculo[]> {

        const vehiculos = await this.vehiculoRepository
          .createQueryBuilder('vehiculo')
          .leftJoinAndSelect('vehiculo.color','color')
          .leftJoinAndSelect('vehiculo.marca','marca')
        .getMany();
    

        // const vehiculos = await this.vehiculoRepository.find();

        if (!vehiculos) {
          throw new NotFoundException();
        } else {
          return vehiculos;
        }
      }
    
      async unicoVehiculo(id: number): Promise<Vehiculo> {
        const vehiculo = await this.vehiculoRepository.findOne(id);
        if (!vehiculo) {
          throw new NotFoundException();
        } else {
          return vehiculo;
        }
      }
    
      async agregarVehiculo(vehiculo: Vehiculo): Promise<Vehiculo> {
        const newVehiculo = await this.vehiculoRepository.save(vehiculo);
        return newVehiculo;
      }
    
      async actualizarVehiculo(id: number, updVehiculo: Vehiculo): Promise<Vehiculo> {
        await this.vehiculoRepository.update(id, updVehiculo);
        return this.unicoVehiculo(id);
      }
    
      eliminarVehiculo(id: number): Promise<DeleteResult> {
        return this.vehiculoRepository.delete(id);
      }
    }
    