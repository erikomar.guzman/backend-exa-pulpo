import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Novedades } from "../entity/Novedades.entity";
import { DeleteResult, Repository } from 'typeorm';

@Injectable()
export class NovedadesRepository{
    constructor(
        @InjectRepository(Novedades)
        private readonly novedadesRepository: Repository<Novedades>,
      ) {}
      async todoNovedades(): Promise<Novedades[]> {
        const novedadess = await this.novedadesRepository.find();
        if (!novedadess) {
          throw new NotFoundException();
        } else {
          return novedadess;
        }
      }
    
      async unicoNovedades(id: number): Promise<Novedades> {
        const novedades = await this.novedadesRepository.findOne(id);
        if (!novedades) {
          throw new NotFoundException();
        } else {
          return novedades;
        }
      }
    
      async agregarNovedades(novedades: Novedades): Promise<Novedades> {
        const newNovedades = await this.novedadesRepository.save(novedades);
        return newNovedades;
      }
    
      async actualizarNovedades(id: number, updNovedades: Novedades): Promise<Novedades> {
        await this.novedadesRepository.update(id, updNovedades);
        return this.unicoNovedades(id);
      }
    
      eliminarNovedades(id: number): Promise<DeleteResult> {
        return this.novedadesRepository.delete(id);
      }
    }
    