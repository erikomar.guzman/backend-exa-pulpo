import { Color } from "src/catalogos/entity/Color.entity";
import { Marca } from "src/catalogos/entity/Marca.entity";
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Novedades } from './Novedades.entity';

@Entity()
export class Vehiculo{

    @PrimaryGeneratedColumn() 
    id: number;

    @Column({unique:true})
    identificacion: string;

    @Column()
    modelo: number;

    @Column()
    fechaIngreso: Date;

    @Column()
    activo: boolean;

    @Column()
    asignado: boolean;

    @ManyToOne(() => Color)
    color: Color;

    @ManyToOne(() => Marca)
    marca: Marca;

    @OneToMany(() => Novedades, novedades => novedades.vehiculo)
    novedaes: Novedades[];
}
