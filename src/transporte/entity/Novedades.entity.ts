import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Vehiculo } from "./Vehiculo.entity";

@Entity('Novedades')
export class Novedades{
    @PrimaryGeneratedColumn() 
    id: number;

    @Column()
    descripcion: string;

    @Column()
    fecha: Date;

    @ManyToOne(()=> Vehiculo)
    vehiculo: Vehiculo;

}
