import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth } from '@nestjs/swagger';
import { RespGenerica } from 'src/dtos/RespGenerica.dto';
import { NovedadesDTO } from './dtos/novedades.dto';
import { VehiculoDTO } from './dtos/vehiculo.dto';
import { Novedades } from './entity/Novedades.entity';
import { Vehiculo } from './entity/Vehiculo.entity';
import { TransporteService } from './transporte.service';

// @ApiBearerAuth()
// @UseGuards(AuthGuard('jwt'))
@Controller('transporte')
export class TransporteController {
    constructor(private transporteService: TransporteService) {}
  
    @Get('/vehiculos')
    async getAllVehiculos(): Promise<Vehiculo[]> {
      return await this.transporteService.obtenerVehiculos();
    }
  
    @Get('/vehiculos/:id')
    getVehiculoById(@Param('id') id: number) {
      return this.transporteService.obtenerVehiculo(id);
    }
    @Post('/vehiculos')
    async addVehiculos(@Body() vehiculoDto: VehiculoDTO): Promise<Vehiculo> {
      vehiculoDto.fechaIngreso = new Date();
      console.log(vehiculoDto);
      return await this.transporteService.agregarVehiculo(vehiculoDto);
    }
    @Put('/vehiculos')
    async updVehiculos(@Body() vehiculoDto: VehiculoDTO): Promise<Vehiculo> {
      return this.transporteService.actualizarVehiculo(vehiculoDto.id, vehiculoDto);
    }
    @Delete('/vehiculos/:id')
    async delVehiculos(@Param('id') id: number): Promise<RespGenerica> {
      const resp = await this.transporteService.eliminarVehiculo(id);
      const respG = new RespGenerica(resp.affected > 0, 'Borrar vehiculo: ' + id);
      return respG;
    }
    //Novedades
    @Get('/Novedades')
    async getAllNovedades(): Promise<Novedades[]> {
      return await this.transporteService.obtenerNovedades();
    }
  
    @Get('/Novedadeses/:id')
    getNovedadesById(@Param('id') id: number) {
      return this.transporteService.obtenerNovedad(id);
    }
    @Post('/Novedadeses')
    async addNovedades(@Body() novedadesDto: NovedadesDTO): Promise<Novedades> {
      return await this.transporteService.agregarNovedades(novedadesDto);
    }
    @Put('/Novedadeses')
    async updNovedades(@Body() novedadesDto: NovedadesDTO): Promise<Novedades> {
      return this.transporteService.actualizarNovedades(novedadesDto.id, novedadesDto);
    }
    @Delete('/Novedadeses/:id')
    async delNovedades(@Param('id') id: number): Promise<RespGenerica> {
      const resp = await this.transporteService.eliminarNovedades(id);
      const respG = new RespGenerica(resp.affected > 0, 'Borrar Novedades: ' + Novedades);
      return respG;
    }
  }
  