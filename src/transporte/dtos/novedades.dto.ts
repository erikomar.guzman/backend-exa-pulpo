import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { Vehiculo } from '../entity/Vehiculo.entity';

export class NovedadesDTO{
    
    @ApiProperty()
    @IsNumber()
    @IsOptional()
    id?: number;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    descripcion: string;

    @ApiProperty()
    @IsDate()
    @IsOptional()
    fecha?: Date;

    @ApiProperty()
    @IsOptional()
    vehiculo?: Vehiculo;
}