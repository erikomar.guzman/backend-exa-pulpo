import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString, IsNotEmpty, IsDate, IsBoolean } from 'class-validator';
import { Color } from 'src/catalogos/entity/Color.entity';
import { Marca } from 'src/catalogos/entity/Marca.entity';

export class VehiculoDTO{
    @ApiProperty()
    @IsNumber()
    @IsOptional()
    id?: number;
    
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    identificacion?: string;
    
    @ApiProperty()
    @IsNumber()
    @IsOptional()
    modelo?: number;
    
    @ApiProperty()
    @IsDate()
    @IsOptional()
    fechaIngreso?: Date;
    
    @ApiProperty()
    @IsBoolean()
    @IsOptional()
    activo?: boolean;
    
    @ApiProperty()
    @IsBoolean()
    @IsOptional()
    asignado?: boolean;
    
    @ApiProperty()
    @IsOptional()
    color: Color;
    
    @ApiProperty()
    @IsOptional()
    marca: Marca;
}