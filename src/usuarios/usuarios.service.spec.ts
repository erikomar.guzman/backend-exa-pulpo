import { Test, TestingModule } from '@nestjs/testing';
import { UsuariosService } from './usuarios.service';
import { Rol } from './entity/Rol.entity';
import { Usuario } from './entity/Usuario.entity';
import { UsuarioDTO } from './dtos/Usuario.dto';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolDTO } from './dtos/Rol.dto';
import { ConfigTest } from '../../test/config';

describe('UsuariosService', () => {
  let service: UsuariosService;
  let idCreacionUser: number;
  let idCreacionRol: number;

  const rol: RolDTO = {
    rol: 0,
    descripcion: 'RolDePruebas',
  };

  const usuario: UsuarioDTO = {
    nombre: 'Andres Juparez',
    usuario: 'UsuarioReservadoParaPruebaEsteNoSeDebeDeCrear004',
    password: 'andres01',
    rol: {
      rol: 1,
      descripcion: 'admin',
    },
  };

  const respUser: Usuario = {
    nombre: 'Andres Juparez',
    usuario: 'andres',
    password: 'andres01',
    rol: {
      rol: 1,
      descripcion: 'admin',
    },
    id: 7,
  };

  beforeEach(async () => {
    const config = new ConfigTest();
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forFeature([Usuario, Rol]),
        TypeOrmModule.forRoot(config.getTypeORM()),
      ],
      providers: [UsuariosService],
      exports: [UsuariosService],
    }).compile();
    service = module.get<UsuariosService>(UsuariosService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create rol', async () => {
    const newRol = await service.agregarRol(rol);
    if (rol) {
      idCreacionRol = newRol.rol;
      usuario.rol = newRol;
    }
    expect(newRol).toBeDefined();
  });

  it('should get rol', async () => {
    const getRol = await service.obtenerRol(idCreacionRol);
    expect(getRol).toBeDefined();
  });

  it('should get All rols', async () => {
    const getAllRols = await service.obtenerRoles();
    expect(getAllRols).toBeDefined();
  });

  it('should update rol', async () => {
    rol.rol = idCreacionRol;
    const updateRol = await service.actualizarRol(idCreacionRol, rol);
    expect(updateRol).toBeDefined();
  });

  it('should create user', async () => {
    const newUser = await service.agregarUsuario(usuario);
    idCreacionUser = newUser.id;
    expect(newUser).toBeDefined();
  });

  it('should get user', async () => {
    const getUser = await service.obtenerUsuario(1);
    expect(getUser).toBeDefined();
  });

  it('should get all users', async () => {
    const getAllUsers = await service.obtenerUsuarios();
    expect(getAllUsers).toBeDefined();
  });

  it('should get update users', async () => {
    const updateUser = await service.actualizarUsuario(idCreacionUser, usuario);
    expect(updateUser).toBeDefined();
  });

  it('should delete user', async () => {
    const deleteUser = await service.eliminarUsuario(idCreacionUser);
    expect(deleteUser).toBeDefined();
  });

  it('should delete rol', async () => {
    const deleteRol = await service.eliminarRol(idCreacionRol);
    expect(deleteRol).toBeDefined();
  });
});
