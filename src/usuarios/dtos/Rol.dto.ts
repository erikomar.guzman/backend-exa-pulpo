import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsOptional, IsString, } from "class-validator";

export class RolDTO{
    
    @ApiProperty()
    @IsNumber()
    @IsOptional()
    rol: number;
 
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    descripcion: string;

    constructor(rol: number, descripcion: string){
        this.rol = rol;
        this.descripcion = descripcion;
    }
}