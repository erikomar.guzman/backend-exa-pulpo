import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth } from '@nestjs/swagger';
import { RespGenerica } from './../dtos/RespGenerica.dto';
import { RolDTO } from './dtos/Rol.dto';
import { UsuarioDTO } from './dtos/Usuario.dto';
import { Rol } from './entity/Rol.entity';
import { Usuario } from './entity/Usuario.entity';
import { UsuariosService } from './usuarios.service';

// @ApiBearerAuth()
// @UseGuards(AuthGuard('jwt'))
@Controller('usuarios')
export class UsuariosController {
  constructor(private usuariosService: UsuariosService) {}


  
  @Get()
  async getAllUsuarios(): Promise<Usuario[]> {
    return await this.usuariosService.obtenerUsuarios();
  }

  @Get('/:id')
  getUsuarioById(@Param('id') id: number) {
    return this.usuariosService.obtenerUsuario(id);
  }
  @Post()
  async addUsuarios(@Body() usuarioDto: UsuarioDTO): Promise<Usuario> {
    return await this.usuariosService.agregarUsuario(usuarioDto);
  }
  @Put()
  async updUsuarios(@Body() usuarioDto: UsuarioDTO): Promise<Usuario> {
    return this.usuariosService.actualizarUsuario(usuarioDto.id, usuarioDto);
  }
  @Delete('/:id')
  async delUsuarios(@Param('id') id: number): Promise<RespGenerica> {
    const resp = await this.usuariosService.eliminarUsuario(id);
    const respG = new RespGenerica(resp.affected > 0, 'Borrar usario: ' + id);
    return respG;
  }
  //ROLES
  @Get('/roles')
  async getAllRoles(): Promise<Rol[]> {
    return await this.usuariosService.obtenerRoles();
  }

  @Get('/roles/:rol')
  getRolByRol(@Param('rol') rol: number) {
    return this.usuariosService.obtenerRol(rol);
  }
  @Post('/roles')
  async addRol(@Body() rolDto: RolDTO): Promise<Rol> {
    return await this.usuariosService.agregarRol(rolDto);
  }
  @Put('/roles')
  async updRol(@Body() rolDto: RolDTO): Promise<Rol> {
    return this.usuariosService.actualizarRol(rolDto.rol, rolDto);
  }
  @Delete('/roles/:rol')
  async delRol(@Param('rol') rol: number): Promise<RespGenerica> {
    const resp = await this.usuariosService.eliminarRol(rol);
    const respG = new RespGenerica(resp.affected > 0, 'Borrar rol: ' + rol);
    return respG;
  }
}
