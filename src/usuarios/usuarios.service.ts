import {
  Injectable,
  ExceptionFilter,
  BadRequestException,
} from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { createQueryBuilder, DeleteResult, Repository } from 'typeorm';
import { RolDTO } from './dtos/Rol.dto';
import { Rol } from './entity/Rol.entity';
import { RolRepository } from './Repositories/Rol.respository';
import { InjectRepository } from '@nestjs/typeorm';
import { UsuariosRepository } from './Repositories/Usuarios.repository';
import { Usuario } from './entity/Usuario.entity';
import { UsuarioDTO, UsuarioLoginDTO } from './dtos/Usuario.dto';

@Injectable()
export class UsuariosService {
  private rolRepository: RolRepository;
  private usuarioRepository: UsuariosRepository;

  constructor(
    @InjectRepository(Rol)
    private readonly repository: Repository<Rol>,
    @InjectRepository(Usuario)
    private readonly repositoryUsuario: Repository<Usuario>,
  ) {
    this.rolRepository = new RolRepository(this.repository);
    this.usuarioRepository = new UsuariosRepository(this.repositoryUsuario);
  }

  //ROLES
  async obtenerRoles(): Promise<Rol[]> {
    const roles: Rol[] = await this.rolRepository.todoRol();
    return roles;
  }

  async obtenerRol(rol: number): Promise<Rol> {
    return await this.rolRepository.unicoRol(rol);
  }

  async agregarRol(rolDto: RolDTO): Promise<Rol> {
    let rol = plainToClass(Rol, rolDto);
    rol = await this.rolRepository.agregarRol(rol);
    return rol;
  }

  async actualizarRol(rol: number, rolDto: RolDTO): Promise<Rol> {
    let updRol = plainToClass(Rol, rolDto);
    updRol = await this.rolRepository.actualizarRol(rol, updRol);
    return updRol;
  }

  async eliminarRol(rol: number): Promise<DeleteResult> {
    const resp = await this.rolRepository.eliminarRol(rol);
    return resp;
  }

  //USUARIOS
  async obtenerUsuarios(): Promise<Usuario[]> {
    const usuarios: Usuario[] = await this.usuarioRepository.todoUsuario();
    return usuarios;
  }

  async obtenerUsuario(id: number): Promise<Usuario> {
    return await this.usuarioRepository.unicoUsuario(id);
  }

  async agregarUsuario(usuarioDto: UsuarioDTO): Promise<Usuario> {
    let usuario = plainToClass(Usuario, usuarioDto);
    usuario = await this.usuarioRepository.agregarUsuario(usuario);
    return usuario;
  }

  async actualizarUsuario(
    id: number,
    usuarioDto: UsuarioDTO,
  ): Promise<Usuario> {
    if (id == null) {
      throw new BadRequestException();
    }
    let updUsuario = plainToClass(Usuario, usuarioDto);
    updUsuario = await this.usuarioRepository.actualizarUsuario(id, updUsuario);
    return updUsuario;
  }

  async eliminarUsuario(id: number): Promise<DeleteResult> {
    const resp = await this.usuarioRepository.eliminarUsuario(id);
    return resp;
  }

  async login(usuario: UsuarioLoginDTO) {
    
    return await this.usuarioRepository.verificarUsuario(
      usuario.usuario,
      usuario.password,
    );
  }
}
