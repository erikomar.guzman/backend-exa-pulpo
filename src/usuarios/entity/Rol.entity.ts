import { Column, Entity, ObjectID, ObjectIdColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity('Rol')
export class Rol {
    @PrimaryGeneratedColumn() 
    rol: number;
    
    @Column({unique: true}) 
    descripcion: string;

    constructor(rol: number, descripcion: string){
        this.rol = rol;
        this.descripcion = descripcion;
    }
}