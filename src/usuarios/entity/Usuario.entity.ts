import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Rol } from "./Rol.entity";

@Entity('Usuario')
export class Usuario{
    @PrimaryGeneratedColumn() 
    id: number;
    
    @Column() 
    nombre: string;

    @Column({unique: true})
    usuario: string;

    @Column()
    password: string;

    @ManyToOne(() => Rol)
    rol: Rol;
}