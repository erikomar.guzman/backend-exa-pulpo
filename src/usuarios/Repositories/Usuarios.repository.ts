import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Usuario } from '../entity/Usuario.entity';
import { Repository, DeleteResult } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { classToPlain } from 'class-transformer';
import { UsuarioRolDTO } from '../dtos/Usuario.dto';

@Injectable()
export class UsuariosRepository {
  constructor(
    @InjectRepository(Usuario)
    private readonly usuarioRepository: Repository<Usuario>,
  ) {}

  salts = 10;

  async todoUsuario(): Promise<Usuario[]> {
    const usuarios = await this.usuarioRepository.find();
    if (!usuarios) {
      throw new NotFoundException();
    } else {
      return usuarios.map((usuario) => {
        usuario.password = undefined;
        return usuario;
      });
    }
  }

  async unicoUsuario(id: number): Promise<Usuario> {
    const usuario = await this.usuarioRepository.findOne(id);
    if (!usuario) {
      throw new NotFoundException();
    } else {
      usuario.password = undefined;
      return usuario;
    }
  }

  async agregarUsuario(usuario: Usuario): Promise<Usuario> {
    usuario.password = await bcrypt.hash(usuario.password, this.salts);
    const newUsuario = await this.usuarioRepository.save(usuario);
    newUsuario.password = undefined;
    return newUsuario;
  }

  async actualizarUsuario(id: number, updUsuario: Usuario): Promise<Usuario> {
    updUsuario.password = await bcrypt.hash(updUsuario.password, this.salts);
    await this.usuarioRepository.update(id, updUsuario);
    return this.unicoUsuario(id);
  }

  eliminarUsuario(id: number): Promise<DeleteResult> {
    return this.usuarioRepository.delete(id);
  }

  async verificarUsuario(usuario: string, pass: string) {
    const usuarioEnc = await this.usuarioRepository
      .createQueryBuilder('usuario')
      .leftJoinAndSelect('usuario.rol','rol')
      .where({usuario})
      .getMany();
    
    if (usuarioEnc.length < 1 ) {
      throw new UnauthorizedException('Sin credenciales');
    } else {
      const isMatch = await bcrypt.compare(pass, usuarioEnc[0].password);
      if (!isMatch) {
        throw new UnauthorizedException('Denegado');
      }
    
       const { password, ...usuarioValido } = usuarioEnc[0];
     return usuarioValido;
    }
  }
}
