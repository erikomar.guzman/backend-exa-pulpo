import { Injectable } from '@nestjs/common';
import { DeleteResult, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Rol } from '../entity/Rol.entity';

@Injectable()
export class RolRepository{
    constructor(
        @InjectRepository(Rol)
        private readonly rolRepository: Repository<Rol>
    ){}

    todoRol(): Promise<Rol[]> {
        return this.rolRepository.find();
    }

    unicoRol(rol: number): Promise<Rol>{
        return this.rolRepository.findOne(rol);
    }

    agregarRol(rol: Rol): Promise<Rol>{
        return this.rolRepository.save(rol);
    }

    async actualizarRol(rol: number, updRol: Rol): Promise<Rol>{
        await this.rolRepository.update(rol, updRol);
        return this.rolRepository.findOne(rol);
    }

    eliminarRol(rol: number): Promise<DeleteResult> {
        return this.rolRepository.delete(rol);
    }
}