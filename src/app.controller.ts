import {
  Controller,
  Get,
  Post,
  UseGuards,
  Body,
} from '@nestjs/common';
import { UsuarioLoginDTO } from './usuarios/dtos/Usuario.dto';
import { AuthService } from './auth/auth.service';

@Controller()
export class AppController {
  constructor(
    private readonly authService :AuthService,
  ) {}

  @Post('auth/login')
  async login(@Body() data: UsuarioLoginDTO) {
    console.log("a: ",data);
    return await this.authService.login(data);
  }
}
