import { Injectable } from '@nestjs/common';
import { UsuarioLoginDTO } from 'src/usuarios/dtos/Usuario.dto';
import { UsuariosService } from '../usuarios/usuarios.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(private usuariosService: UsuariosService,
    private jwtService: JwtService) {}
  
    
  async login(usuario : UsuarioLoginDTO): Promise<any>{
    const payload = await this.usuariosService.login(usuario);
    return {
      token: this.jwtService.sign(payload),
      usuario: payload
    }
  }
  
}
