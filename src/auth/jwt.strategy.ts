import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { jwtConstants } from './constants';
import { UsuariosService } from '../usuarios/usuarios.service';
import { UsuarioDTO } from 'src/usuarios/dtos/Usuario.dto';
import { UnauthorizedException } from '@nestjs/common';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private usuarioService : UsuariosService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }

  async validate(payload: any ): Promise<UsuarioDTO> {
    const user = await this.usuarioService.obtenerUsuario(payload.id);
    if (!user) {
        throw new UnauthorizedException('Sin acceso');
    }
    return user;
  }
}