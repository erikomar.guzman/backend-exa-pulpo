import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsuarioLoginDTO } from 'src/usuarios/dtos/Usuario.dto';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super();
  }

  async validate(usuario: string, password: string): Promise<any> {
    const usuarioDto: UsuarioLoginDTO = {
      usuario,
      password,
    };
    console.log("b: ", usuario, password);
    const user = await this.authService.login(usuarioDto);
    if (!user) {
      throw new UnauthorizedException('No se permite el acceso');
    }
    return user;
  }
}
