
export class RespGenerica{
    isOk: boolean;
    mensaje: string;

    constructor(isOk: boolean, mensaje: string){
        this.isOk = isOk;
        this.mensaje = mensaje;
    }
}