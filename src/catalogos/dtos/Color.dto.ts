import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsNotEmpty, IsString } from 'class-validator';

export class ColorDTO{
    @ApiProperty()
    @IsNumber()
    @IsOptional()
    color: number;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    descripcion: string;

    constructor(color: number, descripcion: string){
        this.color = color;
        this.descripcion = descripcion;
    }
}