import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsNotEmpty, IsString } from 'class-validator';

export class MarcaDTO{

    @ApiProperty()
    @IsNumber()
    @IsOptional()
    marca: number;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    descripcion: string;

    constructor(marca: number, descripcion: string){
        this.marca = marca;
        this.descripcion = descripcion;
    }
}