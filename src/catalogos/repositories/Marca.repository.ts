import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Marca } from '../entity/Marca.entity';
import { Repository, DeleteResult } from 'typeorm';

@Injectable()
export class MarcaRepository{
    constructor(
        @InjectRepository(Marca)
        private readonly MarcaRepository: Repository<Marca>
    ){}

    todoMarca(): Promise<Marca[]> {
        return this.MarcaRepository.find();
    }

    unicoMarca(marca: number): Promise<Marca>{
        return this.MarcaRepository.findOne(marca);
    }

    agregarMarca(marca: Marca): Promise<Marca>{
        return this.MarcaRepository.save(marca);
    }

    async actualizarMarca(marca: number, updMarca: Marca): Promise<Marca>{
        await this.MarcaRepository.update(marca, updMarca);
        return this.MarcaRepository.findOne(marca);
    }

    eliminarMarca(marca: number): Promise<DeleteResult> {
        return this.MarcaRepository.delete(marca);
    }
}