import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Color } from '../entity/Color.entity';
import { DeleteResult, Repository } from 'typeorm';

@Injectable()
export class ColorRepository{
    constructor(
        @InjectRepository(Color)
        private readonly ColorRepository: Repository<Color>
    ){}

    todoColor(): Promise<Color[]> {
        return this.ColorRepository.find();
    }

    unicoColor(color: number): Promise<Color>{
        return this.ColorRepository.findOne(color);
    }

    agregarColor(color: Color): Promise<Color>{
        return this.ColorRepository.save(color);
    }

    async actualizarColor(color: number, updColor: Color): Promise<Color>{
        await this.ColorRepository.update(color, updColor);
        return this.ColorRepository.findOne(color);
    }

    eliminarColor(color: number): Promise<DeleteResult> {
        return this.ColorRepository.delete(color);
    }
}