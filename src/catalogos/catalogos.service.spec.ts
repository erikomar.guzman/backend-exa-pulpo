import { Test, TestingModule } from '@nestjs/testing';
import { CatalogosService } from './catalogos.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Color } from './entity/Color.entity';
import { Marca } from './entity/Marca.entity';
import { ConfigTest } from '../../test/config';
import { ColorDTO } from './dtos/Color.dto';
import { MarcaDTO } from './dtos/Marca.dto';

describe('CatalogosService', () => {
  let service: CatalogosService;

  const color: ColorDTO = {
    color: 0,
    descripcion: 'ColorDePrueba',
  };

  const brand: MarcaDTO = {
    marca: 0,
    descripcion: 'MarcaDePrueba',
  };

  beforeEach(async () => {
    const config = new ConfigTest();
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forFeature([Color, Marca]),
        TypeOrmModule.forRoot(config.getTypeORM()),
      ],
      providers: [CatalogosService],
      exports: [CatalogosService],
    }).compile();

    service = module.get<CatalogosService>(CatalogosService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create rol', async () => {
    const newColor = await service.agregarColor(color);
    if (newColor) {
      color.color = newColor.color;
    }
    expect(newColor).toBeDefined();
  });

  it('should get rol', async () => {
    const getColor = await service.obtenerColor(color.color);
    expect(getColor).toBeDefined();
  });

  it('should get All rol', async () => {
    const getAllColors = await service.obtenerColor(color.color);
    expect(getAllColors).toBeDefined();
  });

  it('should update rol', async () => {
    const getUpdateColor = await service.actualizarColor(color.color, color);
    expect(getUpdateColor).toBeDefined();
  });

  it('should delete rol', async () => {
    const deleteColor = await service.eliminarColor(color.color);
    expect(deleteColor).toBeDefined();
  });

  it('should create brand', async () => {
    const newBrand = await service.agregarMarca(brand);
    if (newBrand) {
      brand.marca = newBrand.marca;
    }
    expect(newBrand).toBeDefined();
  });

  it('should get brand', async () => {
    const getBrand = await service.obtenerMarca(brand.marca);
    expect(getBrand).toBeDefined();
  });

  it('should get all brand', async () => {
    const getAllBrands = await service.obtenerMarcas();
    expect(getAllBrands).toBeDefined();
  });

  it('should update brand', async () => {
    const getUpdateBrand = await service.actualizarMarca(brand.marca, brand);
    expect(getUpdateBrand).toBeDefined();
  });

  it('should delete brand', async () => {
    const deleteBrand = await service.eliminarMarca(brand.marca);
    expect(deleteBrand).toBeDefined();
  });
});
