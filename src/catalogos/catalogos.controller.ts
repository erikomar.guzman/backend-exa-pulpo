import {Body,Controller,Get,Param,Post,Put,Delete, UseGuards,} from '@nestjs/common';
import { RespGenerica } from './../dtos/RespGenerica.dto';
import { CatalogosService } from './catalogos.service';
import { ColorDTO } from './dtos/Color.dto';
import { MarcaDTO } from './dtos/Marca.dto';
import { Color } from './entity/Color.entity';
import { Marca } from './entity/Marca.entity';
import { ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

// @ApiBearerAuth()
// @UseGuards(AuthGuard('jwt'))
@Controller('catalogos')
export class CatalogosController {
  constructor(private catalogoService: CatalogosService) {}

  @Get('/colores')
  async getAllCatalogos(): Promise<Color[]> {
    return await this.catalogoService.obtenerColores();
  }

  @Get('/colores/:color')
  getColorById(@Param('color') color: number) {
    return this.catalogoService.obtenerColor(color);
  }
  @Post('/colores')
  async addColor(@Body() colorDto: ColorDTO): Promise<Color> {
    return await this.catalogoService.agregarColor(colorDto);
  }
  @Put('/colores')
  async actualizarColor(@Body() colorDto: ColorDTO): Promise<Color> {
    return this.catalogoService.actualizarColor(colorDto.color, colorDto);
  }
  @Delete('/colores/:color')
  async delColor(@Param('color') color: number): Promise<RespGenerica> {
    const resp = await this.catalogoService.eliminarColor(color);
    const respG = new RespGenerica(resp.affected > 0, 'Borrar color: ' + color);
    return respG;
  }
  //MarcaES
  @Get('/marcas')
  async getAllMarcaes(): Promise<Marca[]> {
    return await this.catalogoService.obtenerMarcas();
  }

  @Get('/marcas/:marca')
  getMarcaByMarca(@Param('marca') marca: number) {
    return this.catalogoService.obtenerMarca(marca);
  }
  @Post('/marcas')
  async addMarca(@Body() marcaDto: MarcaDTO): Promise<Marca> {
    return await this.catalogoService.agregarMarca(marcaDto);
  }
  @Put('/marcas')
  async updMarca(@Body() marcaDto: MarcaDTO): Promise<Marca> {
    return this.catalogoService.actualizarMarca(marcaDto.marca, marcaDto);
  }
  @Delete('/marcas/:marca')
  async delMarca(@Param('marca') marca: number): Promise<RespGenerica> {
    const resp = await this.catalogoService.eliminarMarca(marca);
    const respG = new RespGenerica(resp.affected > 0, 'Borrar marca: ' + marca);
    return respG;
  }
}
