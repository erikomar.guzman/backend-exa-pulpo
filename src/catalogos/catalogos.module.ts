import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CatalogosController } from './catalogos.controller';
import { CatalogosService } from './catalogos.service';
import { Color } from './entity/Color.entity';
import { Marca } from './entity/Marca.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Color, Marca])],
  controllers: [CatalogosController],
  providers: [CatalogosService],
  exports: [CatalogosService],
})
export class CatalogosModule {}
