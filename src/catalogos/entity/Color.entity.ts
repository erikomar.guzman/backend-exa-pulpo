import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('Color')
export class Color{
    @PrimaryGeneratedColumn()
    color: number;

    @Column({unique:true})
    descripcion: string;

    constructor(color: number, descripcion: string){
        this.color = color;
        this.descripcion = descripcion;
    }
}