import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('Marca')
export class Marca{
    @PrimaryGeneratedColumn()
    marca: number;

    @Column({unique:true})
    descripcion: string;

    constructor(marca: number, descripcion: string){
        this.marca = marca;
        this.descripcion = descripcion;
    }
}