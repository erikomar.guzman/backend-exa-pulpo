import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeleteResult } from 'typeorm';
import { Color } from './entity/Color.entity';
import { ColorRepository } from './repositories/Color.repository';
import { MarcaRepository } from './repositories/Marca.repository';
import { Marca } from './entity/Marca.entity';
import { ColorDTO } from './dtos/Color.dto';
import { plainToClass } from 'class-transformer';
import { MarcaDTO } from './dtos/Marca.dto';

@Injectable()
export class CatalogosService {
  private readonly colorRepository: ColorRepository;
  private readonly MarcaRepository: MarcaRepository;

  constructor(
    @InjectRepository(Color)
    private readonly repositoryColor: Repository<Color>,
    @InjectRepository(Marca)
    private readonly repositoryMarca: Repository<Marca>,
  ) {
    this.colorRepository = new ColorRepository(this.repositoryColor);
    this.MarcaRepository = new MarcaRepository(this.repositoryMarca);
  }

  async obtenerColores(): Promise<Color[]> {
    const colores: Color[] = await this.colorRepository.todoColor();
    return colores;
  }

  async obtenerColor(color: number): Promise<Color> {
    return await this.colorRepository.unicoColor(color);
  }

  async agregarColor(colorDto: ColorDTO): Promise<Color> {
    let color = plainToClass(Color, colorDto);
    color = await this.colorRepository.agregarColor(color);
    return color;
  }

  async actualizarColor(color: number, colorDto: ColorDTO): Promise<Color> {
    let updcolor = plainToClass(Color, colorDto);
    updcolor = await this.colorRepository.actualizarColor(color, updcolor);
    return updcolor;
  }

  async eliminarColor(color: number): Promise<DeleteResult> {
    const resp = await this.colorRepository.eliminarColor(color);
    return resp;
  }

  // MARCA
  async obtenerMarcas(): Promise<Marca[]> {
    const marcas: Marca[] = await this.MarcaRepository.todoMarca();
    return marcas;
  }

  async obtenerMarca(id: number): Promise<Marca> {
    return await this.MarcaRepository.unicoMarca(id);
  }

  async agregarMarca(marcaDto: MarcaDTO): Promise<Marca> {
    let marca = plainToClass(Marca, marcaDto);
    marca = await this.MarcaRepository.agregarMarca(marca);
    return marca;
  }

  async actualizarMarca(id: number, marcaDto: MarcaDTO): Promise<Marca> {
    if (id == null) {
      throw new BadRequestException();
    }
    let updMarca = plainToClass(Marca, marcaDto);
    updMarca = await this.MarcaRepository.actualizarMarca(id, updMarca);
    return updMarca;
  }

  async eliminarMarca(id: number): Promise<DeleteResult> {
    const resp = await this.MarcaRepository.eliminarMarca(id);
    return resp;
  }
}
