import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export class ConfigTest {
  typeORM: TypeOrmModuleOptions = {
    type: 'postgres',
    host: 'gufe.postgres.database.azure.com',
    port: 5432,
    username: 'gufeuser@gufe',
    password: 'Gufe$01$',
    database: 'postgres',
    autoLoadEntities: true,
    synchronize: true,
    keepConnectionAlive: true,
  };

  getTypeORM() {
    return this.typeORM;
  }
}
